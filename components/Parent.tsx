import { useCallback, useState } from "react";
import { Child } from "./Child";

export const Parent = () => {
  console.log("========== Parent");

  const [state1, setState1] = useState(true);

  const handleSwitchChildComponent = useCallback(() => {
    setState1((s) => !s);
  }, []);

  return (
    <>
      <button onClick={handleSwitchChildComponent}>Switch</button>
      {state1 ? (
        <Child key="A" name="A"></Child>
      ) : (
        <Child key="B" name="B"></Child>
      )}
    </>
  );
};
