import React from "react";

export function Child2() {
  console.log("    Child2: render start");

  const [count, setCount] = React.useState(() => {
    console.log("    Child2: useState(() => 0)");
    return 0;
  });

  console.log(`    Child2: count = ${count}`);

  React.useEffect(() => {
    console.log("    Child2: useEffect(() => {})");
    alert("    Child2: useEffect(() => {})");
    return () => {
      console.log("    Child2: useEffect(() => {}) cleanup");
    };
  });

  React.useEffect(() => {
    console.log("    Child2: useEffect(() => {}, [])");
    return () => {
      console.log("    Child2: useEffect(() => {}, []) cleanup");
    };
  }, []);

  React.useEffect(() => {
    console.log("    Child2: useEffect(() => {}, [count])");
    return () => {
      console.log("    Child2: useEffect(() => {}, [count]) cleanup");
    };
  }, [count]);

  React.useLayoutEffect(() => {
    console.log("    Child2: useLayoutEffect(() => {})");
    alert("    Child2: useLayoutEffect(() => {})");
    return () => {
      console.log("    Child2: useLayoutEffect(() => {}) cleanup");
    };
  });

  React.useLayoutEffect(() => {
    console.log("    Child2: useLayoutEffect(() => {}, [])");
    return () => {
      console.log("    Child2: useLayoutEffect(() => {}, []) cleanup");
    };
  }, []);

  React.useLayoutEffect(() => {
    console.log("    Child2: useLayoutEffect(() => {}, [count])");
    return () => {
      console.log("    Child2: useLayoutEffect(() => {}, [count]) cleanup");
    };
  }, [count]);

  const element = (
    <>
      <h2>Child2</h2>
      <button onClick={() => setCount((previousCount) => previousCount + 1)}>
        {count}
      </button>
    </>
  );

  console.log("    Child2: render end");

  return element;
}
