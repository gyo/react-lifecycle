import { FC, useCallback, useEffect, useLayoutEffect, useState } from "react";

type Props = {
  name: string;
};

export const Child: FC<Props> = (props) => {
  console.log("---------- Child", props.name);

  const [state1, setState1] = useState(0);

  const [state2, setState2] = useState(() => {
    console.log("----- useState");
    return 0;
  });

  useLayoutEffect(() => {
    console.log("----- useLayoutEffect");
    return () => {
      console.log("----- cleanup useLayoutEffect");
    };
  });

  useEffect(() => {
    console.log("----- useEffect");
    return () => {
      console.log("----- cleanup useEffect");
    };
  });

  const handleIncrementState1 = useCallback(() => {
    setState1((s) => s + 1);
  }, []);

  return (
    <>
      <h1>{props.name}</h1>
      <p>{state1}</p>
      <button onClick={handleIncrementState1}>Increment</button>
    </>
  );
};
