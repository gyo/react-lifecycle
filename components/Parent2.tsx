import React from "react";
import { Child2 } from "../components/Child2";

export function Parent2() {
  console.log("Parent2: render start");

  const [showChild, setShowChild] = React.useState(() => {
    console.log("Parent2: useState(() => false)");
    return false;
  });

  console.log(`Parent2: showChild = ${showChild}`);

  React.useEffect(() => {
    console.log("Parent2: useEffect(() => {})");
    alert("Parent2: useEffect(() => {})");
    return () => {
      console.log("Parent2: useEffect(() => {}) cleanup");
    };
  });

  React.useEffect(() => {
    console.log("Parent2: useEffect(() => {}, [])");
    return () => {
      console.log("Parent2: useEffect(() => {}, []) cleanup");
    };
  }, []);

  React.useEffect(() => {
    console.log("Parent2: useEffect(() => {}, [showChild])");
    return () => {
      console.log("Parent2: useEffect(() => {}, [showChild]) cleanup");
    };
  }, [showChild]);

  React.useLayoutEffect(() => {
    console.log("Parent2: useLayoutEffect(() => {})");
    alert("Parent2: useLayoutEffect(() => {})");
    return () => {
      console.log("Parent2: useLayoutEffect(() => {}) cleanup");
    };
  });

  React.useLayoutEffect(() => {
    console.log("Parent2: useLayoutEffect(() => {}, [])");
    return () => {
      console.log("Parent2: useLayoutEffect(() => {}, []) cleanup");
    };
  }, []);

  React.useLayoutEffect(() => {
    console.log("Parent2: useLayoutEffect(() => {}, [showChild])");
    return () => {
      console.log("Parent2: useLayoutEffect(() => {}, [showChild]) cleanup");
    };
  }, [showChild]);

  const element = (
    <>
      <h1>Parent2</h1>
      <label>
        <input
          type="checkbox"
          checked={showChild}
          onChange={(e) => setShowChild(e.target.checked)}
        />{" "}
        show child
      </label>
      <div>{showChild ? <Child2 /> : null}</div>
    </>
  );

  console.log("Parent2: render end");

  return element;
}
